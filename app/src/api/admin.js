import axios from 'axios'
import * as constants from '@/constants/api'
// import store from '../store/index'
//
// ─── PATHS ──────────────────────────────────────────────────────────────────────
//

let API_URL = constants.SB_API_URL

const WITHDRAW_LIST = API_URL + '/admin/withdraw/list'
const WITHDRAW_CONFIRM = API_URL + '/admin/withdraw/confirm'

export default {
  getWithdrawList(context) {
    axios
      .post(WITHDRAW_LIST)
      .then(response => {
        context.errorMsg = ''
        context.withdrawals = response.data.data
        console.log(response.data.data)
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
        console.log(e.response)
      })
  },
  confirmWithdraw(context, data) {
    axios
      .post(WITHDRAW_CONFIRM, data)
      .then(response => {
        context.errorMsg = ''
        console.log(response.data.data)
        if (response.data.data.result === true) {
          console.log(data.requestId)
          context.removeRequest(data.requestId)
        }
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
        console.log(e.response)
      })
  }
}
