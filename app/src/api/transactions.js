import axios from 'axios'
import * as constants from '@/constants/api'
//
// ─── PATHS ──────────────────────────────────────────────────────────────────────
//

let API_URL = constants.SB_API_URL

const SEND_REQUEST_URL = API_URL + '/money/withdraw/request'
const SEND_CONFIRM_URL = API_URL + '/money/withdraw/confirm'

export default {
  sendRequest(context, creds) {
    axios
      .post(SEND_REQUEST_URL, creds)
      .then(response => {
        console.log(response.data.data)
        context.isRequested = true
        context.requestId = response.data.data.requestId
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  },
  confirmRequest(context, creds) {
    axios
      .post(SEND_CONFIRM_URL, creds)
      .then(response => {
        if (response.data.data) {
          context.isRequested = false
          context.successMsg = 'NVC sent!'
          context.resetForms()
        }
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  }
}
