import axios from 'axios'
import * as constants from '@/constants/api'
import store from '../store/index'
import Router from '../router/index'

let API_URL = constants.SB_API_URL
/*  API paths */
const LOGIN_URL = API_URL + '/auth/login'
const LOGIN_OTP_URL = API_URL + '/auth/send-otp'
const SIGNUP_URL = API_URL + '/auth/register'
const RESET_REQUEST = API_URL + '/auth/reset-password/request'
const RESET_SEND_CODE = API_URL + '/auth/reset-password/send-code'
const RESET_PASSWORD = API_URL + '/auth/reset-password/send-password'
const USER_STATISTICS = API_URL + '/pub/statistic/user'

function setHeaders(header) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + header
}
function removeHeaders() {
  axios.defaults.headers.common['Authorization'] = ''
}

/*  IF 401 ERROR REDIRECT TO LOGIN  */
axios.interceptors.response.use(
  function(response) {
    return response
  },
  function(error) {
    store.commit('SET_ERROR', '')
    if (
      error.response.request.status === 401 &&
      error.response.config.url !== LOGIN_URL &&
      error.response.config.url !== SIGNUP_URL
    ) {
      methods.logOut(null, { name: 'Login' })
    }
    return Promise.reject(error)
  }
)

let methods = {
  logIn(context, creds, redirect) {
    context.loading = true
    console.log(creds)
    axios
      .post(LOGIN_URL, creds)
      .then(response => {
        if (response.data.data.accessToken !== null) {
          let accessToken = response.data.data.accessToken
          let userId = response.data.data.userId
          setHeaders(accessToken)
          localStorage.setItem('access_token', accessToken)

          store.commit('LOGIN_USER')
          store.commit('SET_ACCESS_TOKEN', accessToken)
          store.commit('SET_USER_ID', userId)
          context.loading = false
          if (redirect) {
            context.$router.push(redirect)
          }
        } else {
          context.otpData = response.data.data
          context.loading = false
        }
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.loading = false
      })
  },
  logInWithOTP(context, creds, redirect) {
    context.loading = true
    axios
      .post(LOGIN_OTP_URL, creds)
      .then(response => {
        if (response.status === 200) {
          let accessToken = response.data.data.accessToken
          let userId = response.data.data.userId
          setHeaders(accessToken)
          localStorage.setItem('access_token', accessToken)
          store.commit('LOGIN_USER')
          store.commit('SET_ACCESS_TOKEN', accessToken)
          store.commit('SET_USER_ID', userId)
          context.loading = false
          if (redirect) {
            context.$router.push(redirect)
          }
        }
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.loading = false
      })
  },
  signUp(context, creds, redirect) {
    context.loading = true
    axios
      .post(SIGNUP_URL, creds)
      .then(response => {
        let accessToken = response.data.data.accessToken
        setHeaders(accessToken)
        localStorage.setItem('access_token', accessToken)

        store.commit('LOGIN_USER')
        store.commit('SET_ACCESS_TOKEN', accessToken)
        store.commit('SET_USER_ID', userId)

        if (redirect) {
          context.$router.push(redirect)
        }
        context.loading = false
      })
      .catch(e => {
        context.loading = false
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  logOut(context, redirect) {
    localStorage.removeItem('access_token')
    store.commit('LOGOUT_USER')
    removeHeaders()
    if (redirect) {
      Router.push(redirect)
    }
  },
  confirm(context, creds, redirect) {
    axios
      .get(CONFIRM_URL, {
        params: creds
      })
      .then(response => {
        store.commit('LOGIN_USER')
        context.loading = false
        if (redirect) {
          context.$router.push(redirect)
        }
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.loading = false
      })
  },
  resetRequest(context, creds) {
    axios
      .post(RESET_REQUEST, creds)
      .then(response => {
        context.resetRequested = true
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  resetSendCode(context, creds) {
    axios
      .post(RESET_SEND_CODE, creds)
      .then(response => {
        context.confirmHash = response.data.data.confirmHash
        context.$router.push({
          name: 'ResetPassword',
          params: {
            confirmHash: response.data.data.confirmHash,
            login: context.login
          }
        })
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  resetPassword(context, creds, redirect) {
    axios
      .post(RESET_PASSWORD, creds)
      .then(response => {
        setHeaders(response.data.data.accessToken)
        localStorage.setItem('access_token', response.data.data.accessToken)

        if (redirect) {
          context.$router.push(redirect)
        }
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  getStats(context) {
    axios.get(USER_STATISTICS).then(response => {
      context.activeUsers = response.data.data.activeUsers
    })
  }
}

export default methods
