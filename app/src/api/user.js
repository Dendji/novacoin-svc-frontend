import axios from 'axios'
import * as constants from '@/constants/api'
import store from '../store/index'
//
// ─── PATHS ──────────────────────────────────────────────────────────────────────
//

let API_URL = constants.SB_API_URL

const USER_INFO_URL = API_URL + '/user/profile'
const WALLET_INFO_URL = API_URL + '/user/info'
const EMAIL_INFO_URL = API_URL + '/email/info'
const EMAIL_CONFIRM_URL = API_URL + '/email/confirm'
const UPDATE_PASSWORD_URL = API_URL + '/user/password'

export default {
  getEmailInfo(context) {
    axios
      .get(EMAIL_INFO_URL)
      .then(response => {
        store.commit('SET_EMAIL_INFO', response.data.data)
        context.emailInfo = response.data.data
        context.errorMsg = ''
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
        store.commit('SET_ERROR', response.data.data.message)
      })
  },
  setEmailInfo(context, data) {
    axios
      .post(EMAIL_INFO_URL, data)
      .then(response => {
        context.emailChanged = response.data.data
        context.errorMsg = ''
        console.log(response.data.data)
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  },
  confirmEmail(context, data) {
    axios
      .post(EMAIL_CONFIRM_URL, data)
      .then(response => {
        context.emailConfirmed = response.data.data
        if (response.data.data) {
          context.notifyConfirmation()
        }
        context.errorMsg = ''
        console.log(response.data.data)
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  },
  getCustomerInfo(context) {
    context.loading = true
    axios
      .get(USER_INFO_URL)
      .then(response => {
        store.commit('SET_CUSTOMER_INFO', response.data.data)
        context.loading = false
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.loading = false
      })
  },
  getWalletInfo(context) {
    axios
      .get(WALLET_INFO_URL)
      .then(response => {
        store.commit('SET_WALLET_INFO', response.data.data)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  updateCustomerInfo(context, data) {
    context.showInfoMessage = false
    context.loading = true
    axios
      .post(USER_INFO_URL, data)
      .then(response => {
        console.log(response)
        context.showInfoMessage = true
        context.loading = false
      })
      .catch(e => {
        context.loading = false
        console.log(e.response)
      })
  },
  updateCustomerPassword(context, creds) {
    context.showInfoMessage = false
    context.loading = true
    axios
      .post(UPDATE_PASSWORD_URL, creds)
      .then(response => {
        console.log(response)
        context.loading = false
        context.passwordChanged = true
      })
      .catch(e => {
        context.loading = false
        console.log(e.response)
        context.passwordError = e.response.data.data.message
      })
  }
}
