import axios from 'axios'
import * as constants from '@/constants/api'
import store from '../store/index'

//
// ─── PATHS ──────────────────────────────────────────────────────────────────────
//

let API_URL = constants.SB_API_URL

const CHANNELS_URL = API_URL + '/notifications/channels'
const EVENTS_URL = API_URL + '/notifications/events'
const TELEGRAM_INFO_URL = API_URL + '/telegram/info'
const TELEGRAM_CONFIRM_URL = API_URL + '/telegram/confirm'
const GOOGLE_AUTH_STATUS = API_URL + '/security/google-auth/status'
const GOOGLE_AUTH_INIT = API_URL + '/security/google-auth/init'
const GOOGLE_AUTH_SET = API_URL + '/security/google-auth/save'
const GOOGLE_AUTH_CANCEL = API_URL + '/security/google-auth/turn-off'

export default {
  getChannels(context) {
    axios
      .get(CHANNELS_URL)
      .then(response => {
        context.channels = response.data.data
        context.errorMsg = ''
        console.log(response.data.data)
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  },
  getEvents(context) {
    axios
      .get(EVENTS_URL)
      .then(response => {
        context.events = response.data.data
        context.errorMsg = ''
        console.log(response.data.data)
      })
      .catch(e => {
        context.errorMsg = e.response.data.data.message
      })
  },
  getGoogleAuthStatus(context) {
    axios
      .get(GOOGLE_AUTH_STATUS)
      .then(response => {
        store.commit('SET_GOOGLE_AUTH_STATUS', response.data.data.work)
        console.log(response.data.data)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  initGoogleAuth(context) {
    axios
      .post(GOOGLE_AUTH_INIT)
      .then(response => {
        context.initAuthData = response.data.data.secret
        console.log(response.data.data)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  setGoogleAuth(context, creds) {
    axios
      .post(GOOGLE_AUTH_SET, creds)
      .then(response => {
        console.log(response.data.data)
        if (response.data.data.result) {
          store.commit('SET_GOOGLE_AUTH_STATUS', true)
        }
        context.googleAuthSetResult = response.data.data.result
        context.result = 'Google Auth has been enabled!'
        store.commit('SET_ERROR', null)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  cancelGoogleAuth(context, code) {
    axios
      .post(GOOGLE_AUTH_CANCEL, code)
      .then(response => {
        if (response.data.data) {
          store.commit('SET_GOOGLE_AUTH_STATUS', false)
        }
        context.googleAuthCancelResult = response.data.data.result
        context.result = 'Google Auth has been disabled!'
        store.commit('SET_ERROR', null)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  setEvent(context, creds) {
    axios
      .post(EVENTS_URL, creds)
      .then(response => {
        context.errorMsg = ''
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.errorMsg = e.response.data.data.message
      })
  },
  activateTelegram(context, data) {
    context.loading = true
    axios
      .post(TELEGRAM_INFO_URL, data)
      .then(response => {
        context.idIsSent = true
        context.telegramInfo = response.data.data
        context.loading = false
      })
      .catch(e => {
        console.log(e.response.data.data)
        context.loading = false
        context.errorMsg = e.response.data.data.message
        store.commit('SET_ERROR', e.response.data.data.message)
      })
  },
  confirmTelegram(data, context) {
    axios
      .post(TELEGRAM_CONFIRM_URL, data)
      .then(response => {
        context.confirmedInfo = response.data.data
        if (response.data.data.status === 'active') {
          context.telegramActivated = true
          context.notifyConfirmation()
        }
        context.loading = false
      })
      .catch(e => {
        context.loading = false
        context.errorMsg = e.response.data.data.message
      })
  },
  getTelegramInfo(context) {
    axios
      .get(TELEGRAM_INFO_URL)
      .then(response => {
        store.commit('SET_TELEGRAM_INFO', response.data.data)
        context.loading = false
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        context.loading = false
      })
  }
}
