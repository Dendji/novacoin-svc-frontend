import axios from 'axios'
import * as constants from '@/constants/api'
import store from '../store/index'

//
// ─── PATHS ──────────────────────────────────────────────────────────────────────
//

let API_URL = constants.SB_API_URL

const TRANSACTIONS_URL = API_URL + '/transactions'
const GET_CURRENCY_RATE = API_URL + '/money/currency/nvc'

export default {
  getCurrencyRate(context) {
    axios
      .get(GET_CURRENCY_RATE)
      .then(response => {
        store.commit('SET_CURRENCY_RATE', response.data.data.price)
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.price)
        context.errorMsg = e.response.data.data.message
      })
  },
  getTransactions(context, page) {
    axios
      .get(TRANSACTIONS_URL, {
        params: {
          page
        }
      })
      .then(response => {
        console.log(response.data)
        store.commit('SET_TRANSACTIONS', response.data.data.objects)
        store.commit('SET_TRANSACTIONS_COUNT', response.data.data.count)
        context.loading = false
      })
      .catch(e => {
        store.commit('SET_ERROR', e.response.data.data.message)
        console.log(e)
      })
  }
}
