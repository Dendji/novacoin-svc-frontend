import Vue from 'vue'
import Router from 'vue-router'
/*  IMPORT PAGES  */
import Login from '@/components/Auth/Login'
import Signup from '@/components/Auth/Signup'
import Panel from '@/components/Dashboard/Index'
import Dashboard from '@/components/Dashboard/Dashboard'
import PageNotFound from '@/components/Pages/PageNotFound'
import Settings from '@/components/Dashboard/Settings/Settings'
import ResetPassword from '@/components/Pages/ResetPassword'
import Reset from '@/components/Pages/Reset'
import SendView from '@/components/Dashboard/SendView'
import AdminView from '@/components/Dashboard/AdminView'
//
// ─── VUEX ───────────────────────────────────────────────────────────────────────
//

import store from '../store/index.js'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: '/login',
      meta: { auth: false }
    },
    {
      path: '/home',
      name: 'Home',
      component: Panel,
      meta: { auth: true }
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword,
      meta: { auth: false }
    },
    {
      path: '/reset',
      name: 'Reset',
      component: Reset,
      meta: { auth: false }
    },
    {
      path: '/dashboard',
      component: Panel,
      meta: { auth: true },
      children: [
        {
          name: 'Dashboard',
          path: '',
          component: Dashboard
          // meta: { auth: true }
        },
        {
          name: 'Settings',
          path: '/settings',
          component: Settings
        },
        {
          name: 'Send',
          path: '/send',
          component: SendView
        },
        {
          name: 'Admin',
          path: '/admin',
          component: AdminView
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { auth: false }
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
      meta: { auth: false }
    },
    {
      path: '*',
      // redirect: '/home',
      name: 'PageNotFound',
      component: PageNotFound,
      meta: { auth: false }
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.isAuth) {
      next({
        path: '/login'
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})
export default router
