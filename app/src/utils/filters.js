import Vue from 'vue'
import moment from 'moment'
export default function() {
  Vue.filter('fiat', function(value) {
    return (Math.round(value * 100) / 100).toFixed(2)
  })
  Vue.filter('usd', function(value) {
    return value + '$'
  })
  Vue.filter('ruble', function(value) {
    return parseFloat(value) + '₽'
  })
  Vue.filter('crypto', function(value) {
    return parseFloat(value).toFixed(8)
  })
  Vue.filter('nvc', function(value) {
    return parseFloat(value).toFixed(4) + ' NVC'
  })
  Vue.filter('month', function(value) {
    return moment(date).format('MMMM')
  })
  Vue.filter('euro', function(value) {
    return parseFloat(value).toFixed(2) + '€'
  })
  Vue.filter('bitcoin', function(value) {
    return parseFloat(value).toFixed(8) + '฿'
  })
  Vue.filter('amount', function(value) {
    return parseFloat(value).toFixed(8)
  })
}
