// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'

import Vue2Filters from 'vue2-filters'
import VeeValidate, { Validator } from 'vee-validate'
import Polyglot from 'vue-polyglot'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'
import { langs } from './lang'
import russian from 'vee-validate/dist/locale/ru'
import VueMask from 'v-mask'
import axios from 'axios'
import initFilters from './utils/filters'

initFilters()
const VeeConfig = {
  fieldsBagName: 'veefields'
}
Validator.localize('ru', russian)
Vue.use(VeeValidate, VeeConfig)
Vue.use(Vue2Filters)
Vue.use(ElementUI, { locale })
Vue.use(BootstrapVue)
Vue.use(VueMask)
Vue.use(Polyglot, {
  defaultLanguage: 'en',
  languagesAvailable: ['ru', 'en']
})
Vue.config.productionTip = false

Vue.locales(langs)

//
// ─── SETTING AUTHORIZATION HEADERS ──────────────────────────────────────────────
//

let token = localStorage.getItem('access_token')
token
  ? (axios.defaults.headers.common['Authorization'] =
      'Bearer ' + localStorage.getItem('access_token'))
  : null

//
// ─── VUE ROOT ───────────────────────────────────────────────────────────────────
//
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  created() {
    this.initLang()
  },
  methods: {
    initLang() {
      if (!localStorage.getItem('lang')) localStorage.setItem('lang', 'en')
      this.$polyglot.setLang({ lang: localStorage.getItem('lang') })
      this.$validator.localize(localStorage.getItem('lang'))
    }
  }
})
