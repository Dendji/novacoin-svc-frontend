import Vue from 'vue'
import Vuex from 'vuex'
// import mutations from './mutations'
Vue.use(Vuex)

const state = {
  appTitle: 'SVC-POOL',
  currencyName: 'NVC',
  currencyRate: '',
  lang: localStorage.getItem('lang'),
  allLangs: ['en', 'ru'],
  isLoggedIn: !!localStorage.getItem('access_token'),
  token: localStorage.getItem('access_token'),
  userId: localStorage.getItem('uid'),
  safelyStoreNumber: 0,
  errorMessage: '',
  dashboardFilter: {
    category: 'All',
    date: '',
    max: '',
    min: '',
    inputMax: '',
    inputMin: ''
  },
  transactions: [],
  emailInfo: {},
  customerInfo: {},
  walletInfo: {},
  telegramInfo: {},
  isGoogleAuthEnabled: null
}
const mutations = {
  LOGIN_USER(state) {
    state.isLoggedIn = true
  },
  LOGOUT_USER(state) {
    state.isLoggedIn = false
    state.userId = null
    state.token = null
  },
  SET_USER_ID(state, uid) {
    state.userId = uid
  },
  SET_ACCESS_TOKEN(state, token) {
    state.accessToken = token
  },
  SET_ERROR(state, error) {
    state.errorMessage = error
  },
  SET_DASHBOARD_FILTER(state, filter) {
    state.dashboardFilter = filter
  },
  SET_EMAIL_INFO(state, info) {
    state.emailInfo = info
  },
  SET_CUSTOMER_INFO(state, info) {
    state.customerInfo = info
  },
  SET_WALLET_INFO(state, info) {
    state.walletInfo = info
  },
  SET_TELEGRAM_INFO(state, info) {
    state.telegramInfo = info
  },
  SET_TELEGRAM_STATUS(state, status) {
    state.telegramInfo.status = status
  },
  SET_CURRENCY_RATE(state, rate) {
    state.currencyRate = rate
  },
  SET_TRANSACTIONS(state, transactions) {
    state.transactions = transactions
  },
  SET_GOOGLE_AUTH_STATUS(state, status) {
    state.isGoogleAuthEnabled = status
  },
  SET_LANG(state, lang) {
    state.lang = lang
  },
  SET_TRANSACTIONS_COUNT(state, count) {
    state.transactionCount = count
  }
}

const actions = {
  LOAD_EMAIL_INFO: function({ commit }) {
    axios
      .get(EMAIL_INFO_URL)
      .then(response => {
        commit('SET_EMAIL_INFO', response.data.data)
        commit('SET_ERROR', '')
      })
      .catch(e => {
        store.commit('SET_ERROR', response.data.data.message)
      })
  }
}
const getters = {
  isAuth: state => {
    return state.isLoggedIn
  },
  getCurrency: state => {
    return state.isLoggedIn
  },
  getTransactions: state => {
    return state.transactions
  },
  getTypes: state => {
    //
    // ─── GETTING ARRAY OF TYPES AND THEN FILTERING UNIQUE VALUE ──────
    //
    return [...new Set(state.transactions.map(item => item.type))]
  }
}
export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
  // modules
})
