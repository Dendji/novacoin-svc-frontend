export default {
  LOGIN_USER(state) {
    state.isLoggedIn = true
  },
  LOGOUT_USER(state) {
    state.isLoggedIn = false
  }
}
