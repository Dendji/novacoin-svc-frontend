FROM zvinger/base-spa-nginx

COPY ./docker/nginx/conf/nginx.conf /etc/nginx/nginx.template.conf
WORKDIR /app

ADD ./app/package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /app && mv /tmp/node_modules /app/node_modules

ADD ./app /app
RUN API_HOST=https://api.svcpool.tk npm run build
RUN cp -a /app/dist /tmp/dist && rm /app/* -R && mv /tmp/dist /app/dist

ENV PROJECT_BASE_URL svcpool.tk
CMD envsubst '$PROJECT_BASE_URL' < /etc/nginx/nginx.template.conf > /etc/nginx/conf.d/site.conf && nginx -g "daemon off;"